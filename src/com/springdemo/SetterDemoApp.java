package com.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterDemoApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		Coach bean=context.getBean("myCricCoach",Coach.class);
		
        System.out.println(bean.getDailyWorkout());
		
		System.out.println(bean.getDailyFortune());
		
		context.close();

	}

}
