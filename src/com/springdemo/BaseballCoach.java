package com.springdemo;

public class BaseballCoach implements Coach {

	FortuneService fortuneService;

	BaseballCoach(){
		
	}
	public BaseballCoach(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
	}

	public String getDailyWorkout() {
		return " Spend 30 min on batting practice";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
	
	//add init method
	void doMyStartUpStuff() {
		System.out.println("Inside doMyStartUpStuff");
	}
	//add destroy method
	void doMyCleanUpStuff() {
		System.out.println("Inside doMyCleanUpStuff");
	}
	
}
