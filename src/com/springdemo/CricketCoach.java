package com.springdemo;

public class CricketCoach implements Coach {

	FortuneService fortuneService;
	
	

	public CricketCoach() {
		System.out.println("Inside no arg constructor");
			}

	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("Inside setter method");
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "hit six's";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}

}
